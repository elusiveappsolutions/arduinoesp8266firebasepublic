
/*
   BM 2021
   https://javafish.blogspot.com/2022/01/sending-temperature-and-humidity-as.html
   
   https://randomnerdtutorials.com/esp32-send-email-smtp-server-arduino-ide/
   https://github.com/mobizt/ESP-Mail-Client/archive/refs/heads/master.zip

   https://randomnerdtutorials.com/esp8266-deep-sleep-with-arduino-ide/
*/

#include <ESP8266WiFi.h>
#include <ESP_Mail_Client.h>

/* Wifi Name and Password */
#define WIFI_SSID "WifiNetworkName"
#define WIFI_PASSWORD "WifiPassword"

#define SMTP_HOST "smtp.gmail.com"
#define SMTP_PORT 465

/* The sign in credentials - Low Security Email - Sender */
#define AUTHOR_EMAIL "CHANGEHERE@gmail.com"
#define AUTHOR_PASSWORD "PASSWORDOFEMAILACCOUNT"

/* Recipient's email*/
#define RECIPIENT1_EMAIL "someemailadres1@gmail.com"
#define RECIPIENT2_EMAIL "someemailadres2@gmail.com"

/* The SMTP Session object used for Email sending */
SMTPSession smtp;

/* Callback function to get the Email sending status */
void smtpCallback(SMTP_Status status);


/**
   BM2021
   Left to Write
   3.3v | Digital Output | Empty | Ground Pin for 4 legged DHT11
*/
#include <dht11.h>
#define dhtInput 2 //GPIO2 of ESP8266-01 
dht11 dht;

void setup() {

  // put your setup code here, to run once:
  Serial.begin(115200); //Start montitoring at 9600 baud
  Serial.setTimeout(2000);

}


void loop() {
  process();
}

/**
 * Functionality of Loop
 */
void process(){
  Serial.println("Started to proccess!");
  
  connectToWifi();

  while(WiFi.status() != WL_CONNECTED)
  {
    //Wait 2 secs and re-try
    delay(2000);
    connectToWifi();
  }

  //Read temperature and send email
  readDHT();
  sendMail();

  //What an hour
  Serial.println("Will Wait for 60 Mins...");

  int sureMiliSec = 1000 * 60 * 60;
  delay(sureMiliSec);

}

/**
   Get Current Readings
*/
void readDHT() {
  Serial.println("\n");
  int chk = dht.read(dhtInput); //0=Okay

  Serial.println((float)dht.humidity, 2);
  Serial.println((float)dht.temperature, 2);
}

/*
   Check Wifi connection and try to connect if not connected
*/
void connectToWifi() {
  Serial.println("Connection Check...");

  if (WiFi.status() != WL_CONNECTED) {
    // connect to wifi.
    Serial.print("connecting");
    delay(1000);

    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    while (WiFi.status() != WL_CONNECTED) {
      Serial.print(".");
      delay(1000);
    }

    Serial.println();
    Serial.print("connected: ");
    Serial.println(WiFi.localIP());
  }
  else {
    Serial.print("Connected IP: ");
    Serial.println(WiFi.localIP());
    Serial.println();
  }
}

/**
   Sends Email
   https://randomnerdtutorials.com/esp32-send-email-smtp-server-arduino-ide/
*/
void sendMail() {

  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("Wifi Connection ERROR!!! Smtp will fail...");
    return;
  }

  /** Enable the debug via Serial port
     none debug or 0
     basic debug or 1
  */
  smtp.debug(1);

  /* Set the callback function to get the sending results */
  smtp.callback(smtpCallback);

  /* Declare the session config data */
  ESP_Mail_Session session;

  /* Set the session config */
  session.server.host_name = SMTP_HOST;
  session.server.port = SMTP_PORT;
  session.login.email = AUTHOR_EMAIL;
  session.login.password = AUTHOR_PASSWORD;
  session.login.user_domain = "";

  /* Declare the message class */
  SMTP_Message message;

  /* Set the message headers */
  message.sender.name = "Ev";
  message.sender.email = AUTHOR_EMAIL;
  message.subject = "Ev Sıcaklık ve Nem";
  
  message.addRecipient("The Queen", RECIPIENT1_EMAIL);
  message.addRecipient("The King", RECIPIENT2_EMAIL);

  /*Send HTML message*/
  String htmlMsg = String("<div style=\"color:#2f4468;\"><h1>Humidity & Temperature</p></div>")
                   + String("<div style=\"color:#2f4468;\"><p> Humidity: ") + String(dht.humidity) + String("</p></div>")
                   + String("<div style=\"color:#2f4468;\"><p> Temperature: ") + String(dht.temperature) + String("</p></div>")
                   + String("<div style=\"color:#2f4468;\"><p> Enjoy Power of Learning!!! </p></div>");

  message.html.content = htmlMsg.c_str();
  message.html.content = htmlMsg.c_str();
  message.text.charSet = "us-ascii";
  message.html.transfer_encoding = Content_Transfer_Encoding::enc_7bit;

  /*
    //Send raw text message
    String textMsg = "Hello World! - Sent from ESP board";
    message.text.content = textMsg.c_str();
    message.text.charSet = "us-ascii";
    message.text.transfer_encoding = Content_Transfer_Encoding::enc_7bit;

    message.priority = esp_mail_smtp_priority::esp_mail_smtp_priority_low;
    message.response.notify = esp_mail_smtp_notify_success | esp_mail_smtp_notify_failure | esp_mail_smtp_notify_delay;*/

  /* Set the custom message header */
  //message.addHeader("Message-ID: <bmiotproject@gmail.com>");

  /* Connect to server with the session config */
  if (!smtp.connect(&session))
  {
    Serial.println("SMTP Connection ERROR!!!, " + smtp.errorReason());
    return;
  }

  /* Start sending Email and close the session */
  if (!MailClient.sendMail(&smtp, &message))
    Serial.println("Error sending Email, " + smtp.errorReason());
}

/* Callback function to get the Email sending status */
void smtpCallback(SMTP_Status status) {
  /* Print the current status */
  Serial.println(status.info());

  /* Print the sending result */
  if (status.success()) {
    Serial.println("----------------");
    ESP_MAIL_PRINTF("Message sent success: %d\n", status.completedCount());
    ESP_MAIL_PRINTF("Message sent failled: %d\n", status.failedCount());
    Serial.println("----------------\n");
    struct tm dt;

    for (size_t i = 0; i < smtp.sendingResult.size(); i++) {
      /* Get the result item */
      SMTP_Result result = smtp.sendingResult.getItem(i);
      time_t ts = (time_t)result.timestamp;
      localtime_r(&ts, &dt);

      ESP_MAIL_PRINTF("Message No: %d\n", i + 1);
      ESP_MAIL_PRINTF("Status: %s\n", result.completed ? "success" : "failed");
      ESP_MAIL_PRINTF("Date/Time: %d/%d/%d %d:%d:%d\n", dt.tm_year + 1900, dt.tm_mon + 1, dt.tm_mday, dt.tm_hour, dt.tm_min, dt.tm_sec);
      ESP_MAIL_PRINTF("Recipient: %s\n", result.recipients);
      ESP_MAIL_PRINTF("Subject: %s\n", result.subject);
    }
    Serial.println("----------------\n");
  }
}
