
/*
	http://javafish.blogspot.com/2021/05/connecting-arduino-to-google-firebase.html

   BM 2021
   5v ->A1
   A2 -> A3
   Gnd -> A5

   Potentiometer
   Left -> Gnd -> E5
   Mid -> Output -> E3
   Input -> E1

  Note: Clone arduino requires drivers to be installed before running.
  https://maker.robotistan.com/arduino-uno-suruculeri-nasil-yuklenir-ch340-cipli-klon/
  Unpin tx rx and reset on upload   
*/


bool DEBUG = true;   //show more logs
int responseTime = 10; //communication timeout


//PH Sensor
int phSensorPin = 1;
int phVal = 0; //Default Potent output
int phUpperLimit = 800;
int phLowerLimit = 240;
int phLedPin = 13;

//Water Level Sensor
int wlSensorPin = 2;
int wlVal = 0; //Default Potent output
int wlUpperLimit = 1000;
int wlLowerLimit = 250;

//Pumps
int ledPinDirtyPump = 11;
int ledPinCleanPump = 12;

//Flags
int PH_COUNT_TRESHOLD = 60; //ardışık 10 okuma sonrası PH hatası
int cycleSecs = 1000; //10 sn

int  phCriticalCount = 0;
bool waterChangeInProcess = false;//Şu anda su değişimi yapılıyor ise
bool emptyingAqua = false; //Akvaryum boşaltma süreci aktif ise
bool fillingAqua = false; //Akvaryum doldurma süreci aktif ise

String tag = "";

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); //Start montitoring at 9600 baud
  pinMode(phLedPin, OUTPUT);
  pinMode(ledPinDirtyPump, OUTPUT);
  pinMode(ledPinCleanPump, OUTPUT);

  /*
    sendToWifi("AT+CWMODE=2",responseTime,DEBUG); // configure as access point
    sendToWifi("AT+CIFSR",responseTime,DEBUG); // get ip address
    sendToWifi("AT+CIPMUX=1",responseTime,DEBUG); // configure for multiple connections
    sendToWifi("AT+CIPSERVER=1,80",responseTime,DEBUG); // turn on server on port 80
  */
}

void loop() {
  //Serial.println("");
  //Serial.println("");

  // Process PH Data
  phVal = analogRead(phSensorPin);
  //Serial.print("PH:");
  //Serial.println(phVal); 
  
  tag = "CMD:; ID:A0001 ; PH: ";
  tag = tag + phVal;


  if (phVal > phUpperLimit || phVal < phLowerLimit)
  {
    phCriticalCount++;
    digitalWrite(phLedPin, HIGH);
    //Serial.print("PH Limit!!!! PH:");
    //Serial.println(phVal);
  } else {
    phCriticalCount = 0;

    digitalWrite(phLedPin, LOW);
    //Serial.print("PH OK. PH:");
    //Serial.println(phVal);
  }

  tag = tag + "; PH Critical Count: ";
  tag = tag + phCriticalCount;

  // Process Water Level Data
  wlVal = analogRead(wlSensorPin);
  //Serial.print("Water Level :");
  //Serial.println(wlVal);

  tag = tag + "; Water Level: ";
  tag = tag + wlVal;

  //Su değişimi başlasın mı?
  if (!waterChangeInProcess)
  {
    if (phCriticalCount >= PH_COUNT_TRESHOLD) {
      waterChangeInProcess = true;
      emptyingAqua = true;
      fillingAqua = false;
      phCriticalCount = 0;
      //Serial.print("Su değişimi başlatılıyor....");
    }
  } else {

    //Serial.print("Su değişimi devam ediyor....");

    //Su değişimi boyunca PH sayma
    phCriticalCount = 0;

    //Boşaltım var ise ve su seviyesi yüksek ise
    if (emptyingAqua) {
      if (wlVal > wlLowerLimit) {
        //Serial.print("Boşaltılıyor....");
        //Boşaltım pompası açık kalsın
        digitalWrite(ledPinDirtyPump, HIGH);

        //Doldurma kapalı
        fillingAqua = false;
      } else {
        //Serial.print("Boşaltım bitti!");
        //Boşaltmayı sonlandır
        digitalWrite(ledPinDirtyPump, LOW);
        emptyingAqua = false;

        //Doldurmayı aç
        fillingAqua = true;
      }
    }

    //Su dolumuna devam edilsin mi
    if (fillingAqua)
    {
      //doluma devam
      if (wlVal < wlUpperLimit)
      {
        //Serial.print("Dolduruluyor...");
        digitalWrite(ledPinCleanPump, HIGH);
      } else {
        //Serial.print("Su değişimi bitti...");
        //dolumu ve su değişimini sonlandır
        digitalWrite(ledPinCleanPump, LOW);
        fillingAqua = false;
        waterChangeInProcess = false;
      }
    }
  }


  if (waterChangeInProcess == true)
    tag = tag + "; Water Critical : 1";
  else
    tag = tag + "; Water Critical : 0";

  if (emptyingAqua == true)
    tag = tag + "; Emptying Tank : 1";
  else
    tag = tag + "; Emptying Tank : 0";

  if (fillingAqua == true)
    tag = tag + "; Filling Tank : 1";
  else
    tag = tag + "; Filling Tank : 0";


  //Send tag to esp8266   
  Serial.flush();
  Serial.println(tag);
  
  //Read from esp8266
  //tag = readSerialMessage();  
  //Serial.println("");    

  delay(cycleSecs);
}

/*
  Name: find
  Description: Function used to match two string
  Params:
  Returns: true if match else false
*/
boolean find(String string, String value) {
  return string.indexOf(value) >= 0;
}


/*
  Name: readSerialMessage
  Description: Function used to read data from Arduino Serial.
  Params:
  Returns: The response from the Arduino (if there is a reponse)
*/
String  readSerialMessage() {
  char value[100];
  int index_count = 0;
  while (Serial.available() > 0) {
    value[index_count] = Serial.read();
    index_count++;
    value[index_count] = '\0'; // Null terminate the string
  }
  String str(value);
  str.trim();
  return str;
}
