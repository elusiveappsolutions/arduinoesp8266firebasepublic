
/*

http://javafish.blogspot.com/2021/05/connecting-arduino-to-google-firebase.html	

  1- To upload program to ESP8266 (To Activate Upload Mode)
    -> Plug Arduino Reset to Ground
    -> ESP8266 Plug GPIO0 to Ground     
    -> Connect (sol alt)TX -> TX, RX(sağ üst) -> RX (That is just to upload code, for normal serial communication between arduino and esp8266, Connect RX to TX and TX to RX)
    and then plug usb to pc
    Upload program, plug un plug usb if not loading
    Once Hard Reset is Sent via pin program is uploaded.
    
    To Run ESP8266
    -> Keep Arduino Reset connected to Ground
    -> UNPLUG GPIO0 - not plugged anywhere
   Check different baud rates on serial monitor and then select 9600

   To Run Arduino
   -> Unplug reset from ground, leave un plugged. Now both Arduino and ESP8266 will be running

  2-To Connect firebase -> change header file finger print -> FirebaseHTTPClient.h
  static const char kFirebaseFingerprint[] =
      "YOUR OWN FINGER PRINT";

  3-For serial communication between arduino and esp8266, Connect RX to TX and TX to RX
*/

#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>

#include <SoftwareSerial.h>

// Set these to run example.
#define FIREBASE_HOST "aquarium-yourOwnDB-default-rtdb.firebaseio.com"
#define FIREBASE_AUTH "YOUR OWN FINGER PRINT"
//https://www.grc.com/fingerprints.htm
//https://github.com/FirebaseExtended/firebase-arduino/issues/474

#define WIFI_SSID "VODAFONE_298B"
#define WIFI_PASSWORD "YOUR OWN WIFI PASSS"

int comCnt = 0;
int dbCnt = 0;
int msgCnt = 0; //Kaç mesajda bir göndereceğiz.

SoftwareSerial wifiSerial(3, 1);      // RX, TX for ESP8266
//https://iot-guider.com/esp8266-nodemcu/serial-communication-between-esp8266-and-arduino/#:~:text=SoftwareSerial%20allows%20serial%20communication%20on,ESP8266%20works%20on%20that%20rate.

void setup() {
  
  Serial.begin(9600);
  Serial.println("setup...");  
  
  wifiSerial.begin(9600);

  connectToWifi();

  //Set Firebase Params
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
}

void loop() {

  //Check incoming data
  if (wifiSerial.available() > 0) {
    String message = readSerialMessage();    
    //Serial.print("Received:");
    //Serial.println(message);

    //Send to uno
    //wifiSerial.print("Received:");
    //wifiSerial.println(message);

    if (find(message, "CMD:")) { 

      //Her 10 mesajdan birini yollayacağız. 10 sn de bir. uno 1 sn lik döngüde      
      if(msgCnt >= 10)      
      {
        writeToDB(message);
        msgCnt = 0;
      }
      
      msgCnt++;
    }
  }  

  //Her 30 sn'de bağlantı durumunu güncelle
  comCnt++;
  if(comCnt >= 3000)
  {
    comCnt = 0;
    Serial.println("Com Check...");  
    writeToDB("CMD:ID:A0001CC:COM_CHECK_OK");    
  }
  
  delay(10);
}

/**
 * Write data to tags on Firebase
 */
void writeToDB(String cmd){
  connectToWifi();

  dbCnt++;
  if(dbCnt > 30)
  {
    dbCnt = 0;
    Firebase.remove("tags");
  }

  String res = Firebase.pushString("tags", cmd);  
  
  // handle error
  if (Firebase.failed()) {
    Serial.print("failed adding tags ->");
    Serial.println(Firebase.error());
    return;
  }else{
    Serial.print("Written to DB:");  
    Serial.println(cmd);  
  }  
}


/*
 * Check Wifi connection and try to connect if not connected
 */
void connectToWifi() {

  if (WiFi.status() != WL_CONNECTED) {

    // connect to wifi.
    Serial.print("connecting");
    delay(1000);
    
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    while (WiFi.status() != WL_CONNECTED) {
      Serial.print(".");
      delay(1000);
    }

    Serial.println();
    Serial.print("connected: ");
    Serial.println(WiFi.localIP());
  }
  else {
    Serial.print("Connected IP: ");
    Serial.println(WiFi.localIP());
    Serial.println();
  }
}


/*
* Name: find
* Description: Function used to match two string
* Params: 
* Returns: true if match else false
*/
boolean find(String string, String value){
  return string.indexOf(value)>=0;
}


/*
* Name: readSerialMessage
* Description: Function used to read data from Arduino Serial.
* Params: 
* Returns: The response from the Arduino (if there is a reponse)
*/
String  readSerialMessage(){
  char value[200]; 
  int index_count =0;
  while(wifiSerial.available()>0){
    delay(2);
    value[index_count]= wifiSerial.read();
    index_count++;
    value[index_count] = '\0'; // Null terminate the string
  }
  String str(value);
  str.trim();
  return str;
}
